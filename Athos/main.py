import argparse
from argparse import RawTextHelpFormatter

def parse_args():
    parser = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter)
    parser.add_argument(
        "--source",
        required=True,
        type=str
    )
    args = parser.parse_args()
    return args




# input_path = "ResNet_64_aby0.cpp"
# output_path = "ModifiedResNet_64_aby0.cpp"
function_start = "void Relu(int"
replacement_path = "new_relu.cpp"


if __name__ == "__main__":

    args = parse_args()

    input_path = args.source
    output_path = "Modified"+input_path

    with open(replacement_path, "r") as f:
        replacement_lines = f.readlines()
        replacement_lines.append("\n") # To avoid overlapping functions in the output

    with open(input_path, "r") as f:
        lines = f.readlines()

    start = 0
    lefts = 0
    rights = 0
    for i in range(len(lines)):
        if lines[i].startswith(function_start):
            start = i
            lefts = lines[i].count("{")
            rights = lines[i].count("}")
            break
    end = start + 1
    while lefts > rights:
        lefts += lines[end].count("{")
        rights += lines[end].count("}")
        end += 1

    print("\n".join(lines[start:end]))
    new_lines = lines[:start]+replacement_lines+lines[end:]
    with open(output_path, "w+") as f:
        f.writelines(new_lines)